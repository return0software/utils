#include <stdio.h>

#include "util.h"


void increment(int *i)
{
    *i = *i + 1;
}

int main()
{
    int arr[5] = {1, 2, 3, 4, 5};
    map_int(arr, ARRAY_SIZE(arr), increment);

    for (unsigned int i = 0; i < ARRAY_SIZE(arr); i++) {
        printf("%d\n", arr[i]);
    }

    return 0;
}
