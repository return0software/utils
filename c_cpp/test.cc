#include <iostream>
#include <vector>

#include "util.hh"

void increment(int *i)
{
    *i = *i + 1;
}

int main()
{
    int arr[5] = {1, 2, 3, 4, 5};
    map(arr, ARRAY_SIZE(arr), increment);

    map(arr, ARRAY_SIZE(arr), [](int *a) {
                                    *a = *a + 1;
                                });

    for (unsigned int i = 0; i < ARRAY_SIZE(arr); i++) {
        std::cout << arr[i] << '\n';
    }

    return 0;
}
