#ifndef _UTIL_H_
#define _UTIL_H_

#include <stdlib.h>
#include <stdio.h>

// Return the size of an array. Note that arrays decay to pointers.
#define ARRAY_SIZE(x)   sizeof(x) / sizeof((x)[0])

/*
 * This is the template of how to write a JS-like map function in C.
 * The first argument is an array. Next argument is the size of the array.
 * The last argument is a function pointer that takes as an argument a
 * pointer to the array you passed into the map function at some index.
 * Take this example:
 * 
 *      int array[5] = {1, 2, 3, 4, 5};
 *      void increment(int *i)
 *      {
 *          *i = *i + 1;
 *      }
 * 
 * The map function would be called like this:
 * 
 *      map_int(array, ARRAY_SIZE(arr), increment);
 * 
 */
void map_int(int *array, unsigned int size, void (*f)(int*))
{
    for (unsigned int i = 0; i < size; i++) {
        (*f)(array + i);
    }
}

#endif // _UTIL_H_
