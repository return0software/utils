#ifndef _UTIL_HH_
#define _UTIL_HH_

#include <functional>

// Return the size of an array. Note that arrays decay to pointers.
#define ARRAY_SIZE(x)   sizeof(x) / sizeof((x)[0])

/*
 * This is the template of how to write a JS-like map function in C++.
 * The first argument is an array. Next argument is the size of the array.
 * The last argument is a function pointer/lambda function that takes as
 * an argument a pointer to the array you passed into the map function at
 * some index.
 * 
 * Take this example:
 * 
 *      int array[5] = {1, 2, 3, 4, 5};
 *      void increment(int *i)
 *      {
 *          *i = *i + 1;
 *      }
 * 
 * The map function would be called like this:
 * 
 *      map(array, ARRAY_SIZE(arr), increment);
 * 
 * or this if you are using a lambda expression:
 * 
 *      map(arr, ARRAY_SIZE(arr), [](int *a) {
 *                                      *a = *a + 1;
 *                                  });
 */

template<class T, class F>
void map(T *array, unsigned int size, F func)
{
    for (unsigned int i = 0; i < size; i++) {
        func(array + i);
    }
}

#endif // _UTIL_HH_
